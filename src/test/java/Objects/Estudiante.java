package Objects;

import java.util.ArrayList;

public class Estudiante {
    public String nombre;
    public String profesion;
    public ArrayList<Experiencia> experiencia;
    public ArrayList<Educacion> educacion;
    public ArrayList<Certificaciones> certificaciones;
    public ArrayList<String> conocimientos;

    public Estudiante(String nombre, String profesion, ArrayList<Experiencia> experiencia, ArrayList<Educacion> educacion,
                      ArrayList<Certificaciones> certificaciones, ArrayList<String> conocimientos) {
        this.nombre = nombre;
        this.profesion = profesion;
        this.experiencia = experiencia;
        this.educacion = educacion;
        this.certificaciones = certificaciones;
        this.conocimientos = conocimientos;
    }
}
