package com.linkedIn.utils;

import Objects.Estudiante;
import com.google.gson.Gson;

import java.io.*;
import java.util.ArrayList;

public class WriteFile {
    public WriteFile() {
        // TODO Auto-generated constructor stub
    }

    public void write(ArrayList<Estudiante> listEstudiante) throws IOException {
        try {
            String filepath = "files/Archivos/respuesta.json";
            //String filepath = "C:/Users/00900/Documents/Proyecto de grado/proyecto/proyecto/src/assets/models/respuesta.json";
            Gson gson = new Gson();
            String nuevo = gson.toJson(listEstudiante);
            File file = new File(filepath);
            if (!file.exists()) {
                file.createNewFile();
                FileWriter fw = new FileWriter(file);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(nuevo);
                bw.newLine();
                bw.close();
            } else {
                FileWriter fw = new FileWriter(file);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(nuevo);
                bw.newLine();
                bw.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
