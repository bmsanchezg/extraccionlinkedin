package com.linkedIn.tests;

import com.linkedIn.pages.ExtractInfo;
import com.linkedIn.pages.FindStudents;
import com.linkedIn.utils.BeforeAfter;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

public class ExtractTest extends BeforeAfter {

    @Test
    public void Extract() throws InterruptedException, IOException, AWTException {

        FindStudents findStudents = new FindStudents(driver);
        String url = "https://www.linkedin.com";
        findStudents.find(url);
        ExtractInfo extractInfo = new ExtractInfo(driver);
        extractInfo.List();

    }

}
