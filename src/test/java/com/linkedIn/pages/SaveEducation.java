package com.linkedIn.pages;

import Objects.Educacion;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.awt.*;
import java.util.ArrayList;

public class SaveEducation {
    WebDriver driver;

    public SaveEducation(WebDriver remoteDriver) {
        driver = remoteDriver;
        PageFactory.initElements(driver, this);
    }

    public void Save(ArrayList<Educacion> listEducation) throws InterruptedException, AWTException {
        Educacion educacion;
        Thread.sleep(3000);
        try {
            WebElement mas = driver.findElement(By.xpath("//div[@id='education']//parent::section/div/div[@class='pvs-list__footer-wrapper']//a"));
            mas.click();
            Thread.sleep(3000);
            String Size = String.valueOf(driver.findElements(By.xpath("//ul/li//div[@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']/a")).size());
            int i = 0, aux = 1;
            do {
                try {
                    String universidad;
                    try {
                        universidad = driver.findElement(By.xpath("//ul/li[" + (aux) + "]//div[@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']/a/div/span/span[@aria-hidden='true']")).getText();
                    }catch (Exception e){
                        universidad="";
                    }
                    String titulo;
                    try{
                        titulo = driver.findElement(By.xpath("//ul/li[" + (aux) + "]//div[@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']/a/span[1]/span[@aria-hidden='true']")).getText();
                    }catch (Exception e){
                        titulo="";
                    }
                    String fecha, fechaI, fechaF;
                    try {
                        fecha = driver.findElement(By.xpath("//ul/li[" + (aux) + "]//div[@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']/a/span[2]/span[@aria-hidden='true']")).getText();
                        if (fecha.contains("-")) {
                            String fechaC[] = fecha.split("-");
                            fechaI = fechaC[0].trim();
                            fechaF = fechaC[1].trim();
                        } else {
                            fechaI = fecha;
                            fechaF = "Actualidad";
                        }

                    } catch (Exception e1) {
                        fechaI = "";
                        fechaF = "";
                    }
                    educacion = new Educacion(universidad, titulo, fechaI, fechaF);
                    listEducation.add(educacion);
                    i++;
                    aux++;
                } catch (Exception e) {
                    aux++;
                }
            } while (i < Integer.parseInt(Size));
            Thread.sleep(3000);
            driver.navigate().back();
        } catch (Exception e) {
            try {
                String Size = String.valueOf(driver.findElements(By.xpath("//div[@id='education']//following-sibling::div[2]/ul/li//div[@class='display-flex flex-column full-width align-self-center']//a")).size());
                int i = 0, aux = 1;
                do {
                    try {
                        String universidad;
                        try {
                            universidad = driver.findElement(By.xpath("//div[@id='education']//following-sibling::div[2]/ul/li[" + (aux) + "]//div[@class='display-flex flex-column full-width align-self-center']//a/div/span/span[@aria-hidden='true']")).getText();
                        }catch (Exception e2){
                            universidad="";
                        }
                        String titulo;
                        try {
                            titulo = driver.findElement(By.xpath("//div[@id='education']//following-sibling::div[2]/ul/li[" + (aux) + "]//div[@class='display-flex flex-column full-width align-self-center']//a/span[1]/span[@aria-hidden='true']")).getText();
                        }catch (Exception e2){
                            titulo="";
                        }
                        String fecha, fechaI, fechaF;
                        try {
                            fecha = driver.findElement(By.xpath("//div[@id='education']//following-sibling::div[2]/ul/li[" + (aux) + "]//div[@class='display-flex flex-column full-width align-self-center']//a/span[2]/span[@aria-hidden='true']")).getText();
                            if (fecha.contains("-")) {
                                String fechaC[] = fecha.split("-");
                                fechaI = fechaC[0].trim();
                                fechaF = fechaC[1].trim();
                            } else {
                                fechaI = fecha;
                                fechaF = "Actualidad";
                            }
                        } catch (Exception e1) {
                            fechaI = "";
                            fechaF = "";
                        }
                        educacion = new Educacion(universidad, titulo, fechaI, fechaF);
                        listEducation.add(educacion);
                        i++;
                        aux++;
                    } catch (Exception ex) {
                        aux++;
                    }
                } while (i < Integer.parseInt(Size));
            } catch (Exception e1) {
            }
        }
    }
}
