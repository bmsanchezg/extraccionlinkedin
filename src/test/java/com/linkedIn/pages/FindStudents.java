package com.linkedIn.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.awt.*;

public class FindStudents {
    WebDriver driver;

    public FindStudents(WebDriver remoteDriver) {
        driver = remoteDriver;
        PageFactory.initElements(driver, this);
    }

    public void find(String link) throws InterruptedException, AWTException {
        driver.get(link);
        driver.findElement(By.xpath("//a[@class='nav__button-secondary btn-md btn-secondary-emphasis']")).click();
        WebElement user = driver.findElement(By.id("username"));
        user.sendKeys("sbrayan584@gmail.com");
        WebElement pass = driver.findElement(By.id("password"));
        pass.sendKeys("Miguel/m30");
        driver.findElement(By.xpath("//button[@class='btn__primary--large from__button--floating']")).click();
        driver.get("https://www.linkedin.com/school/universidad-distrital-francisco-jos%C3%A9-de-caldas/");
        Thread.sleep(2000);
        driver.findElement(By.xpath("//span[@class='t-normal t-black--light link-without-visited-state link-without-hover-state']")).click();
        Thread.sleep(5000);
    }
}
