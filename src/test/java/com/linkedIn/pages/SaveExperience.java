package com.linkedIn.pages;

import Objects.Experiencia;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.awt.*;
import java.util.ArrayList;

public class SaveExperience {
    WebDriver driver;

    public SaveExperience(WebDriver remoteDriver) {
        driver = remoteDriver;
        PageFactory.initElements(driver, this);
    }

    public void Save(ArrayList<Experiencia> listExperiencia) throws InterruptedException, AWTException {
        Experiencia experiencia;
        Thread.sleep(3000);
        try {
            WebElement mas = driver.findElement(By.xpath("//div[@id='experience']//parent::section/div/div[@class='pvs-list__footer-wrapper']//a"));
            mas.click();
            Thread.sleep(3000);
            String experSize = String.valueOf(driver.findElements(By.xpath("//ul/li[@class='pvs-list__paged-list-item artdeco-list__item pvs-list__item--line-separated ']/div/div/div/a")).size());
            int i = 1, aux = 1;
            do {
                try {
                    String cargo;
                    try {
                        cargo = driver.findElement(By.xpath("//ul/li[" + (aux) + "]//div [@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']/div/div/parent::div/div/span//span[@aria-hidden='true']")).getText();
                    }catch (Exception e){
                        cargo="";
                    }
                    String empresa;
                    try {
                        empresa = driver.findElement(By.xpath("//ul/li[" + (aux) + "]//div [@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']/div/div/parent::div/span[1]/span[@aria-hidden='true']")).getText();
                        if (empresa.indexOf("·") != -1)
                            empresa = empresa.substring(0, empresa.indexOf("·"));
                    }catch (Exception e){
                        empresa="";
                    }
                    String fecha, fecha1, fecha2;
                    try {
                        fecha = driver.findElement(By.xpath("//ul/li[" + (aux) + "]//div [@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']/div/div/parent::div/span[2]/span[@aria-hidden='true']")).getText();
                        if (fecha.contains("-")) {
                            String fechaC[] = fecha.split("-");
                            fecha1 = fechaC[0].trim();
                            fecha2 = fechaC[1].trim();
                            if (fecha2.indexOf("·") != -1)
                                fecha2 = fecha2.substring(0, fecha2.indexOf("·"));
                        } else {
                            fecha1 = fecha;
                            fecha2 = "Actualidad";
                        }
                    } catch (Exception e1) {
                        fecha1 = "";
                        fecha2 = "";
                    }
                    experiencia = new Experiencia(empresa, cargo, fecha1, fecha2);
                    listExperiencia.add(experiencia);
                    i++;
                    aux++;
                } catch (Exception e) {
                    try {
                        String experSize2 = String.valueOf(driver.findElements(By.xpath("//ul/li/div/div/div [@class='display-flex flex-column full-width align-self-center']//div[@class='pvs-list__outer-container']/ul/li/div/div[@class]/div/ul")).size());
                        String tam = String.valueOf(driver.findElements(By.xpath("//ul/li[" + aux + "]/div/div/div [@class='display-flex flex-column full-width align-self-center']//div[@class='pvs-list__outer-container']/ul/li/div/div[@class]/div/ul/li")).size());
                        String empresa;
                        try {
                            empresa = driver.findElement(By.xpath("//ul/li[" + aux + "]/div/div/div [@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']/a/div/span//span[@aria-hidden='true']")).getText();
                            if (empresa.indexOf("·") != -1)
                                empresa = empresa.substring(0, empresa.indexOf("·"));
                        }catch (Exception e1){
                            empresa="";
                        }
                        for (int j = 0; j < Integer.parseInt(tam); j++) {
                            String cargo;
                            try {
                                cargo = driver.findElement(By.xpath("//ul/li[" + aux + "]/div/div/div [@class='display-flex flex-column full-width align-self-center']/div[@class='pvs-list__outer-container']//div[@class='scaffold-finite-scroll__content']/ul/li[" + (j + 1) + "]//div[@class='display-flex flex-row justify-space-between']/a/divspan//span[@aria-hidden='true']")).getText();
                            }catch (Exception e1) {
                                cargo="";
                            }

                            String fecha, fecha1, fecha2;
                            try {
                                fecha = driver.findElement(By.xpath("//ul/li[" + aux + "]/div/div/div [@class='display-flex flex-column full-width align-self-center']/div[@class='pvs-list__outer-container']//div[@class='scaffold-finite-scroll__content']/ul/li[" + (j + 1) + "]//div[@class='display-flex flex-row justify-space-between']/a/span[1]")).getText();
                                if (fecha.contains("-")) {
                                    String fechaC[] = fecha.split("-");
                                    fecha1 = fechaC[0].trim();
                                    fecha2 = fechaC[1].trim();
                                    if (fecha2.indexOf("·") != -1)
                                        fecha2 = fecha2.substring(0, fecha2.indexOf("·"));
                                } else {
                                    fecha1 = fecha;
                                    fecha2 = "Actualidad";
                                }
                            } catch (Exception e1) {
                                fecha1 = "";
                                fecha2 = "";
                            }
                            experiencia = new Experiencia(empresa, cargo, fecha1, fecha2);
                            listExperiencia.add(experiencia);
                        }
                    } catch (Exception v) {
                    }
                    aux++;
                }
            } while (i < Integer.parseInt(experSize));
            Thread.sleep(3000);
            driver.navigate().back();
        } catch (Exception e) {
            Thread.sleep(3000);
            try{
            String experSize = String.valueOf(driver.findElements(By.xpath("//div[@id='experience']//parent::section/div/div//parent::div[@class='pvs-header__container']/following-sibling::div/ul/li//div[@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']/div/div/parent::div")).size());
            int i = 1, aux = 1;
            do {
                try {
                    String cargo;
                    try{
                        cargo = driver.findElement(By.xpath("//div[@id='experience']//following-sibling::div[2]/ul/li[" + (aux) + "]//div [@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']/div/div/parent::div/div/span//span[@aria-hidden='true']")).getText();
                    }catch (Exception e1){
                        cargo="";
                    }
                    String empresa;
                    try{
                        empresa = driver.findElement(By.xpath("//div[@id='experience']//following-sibling::div[2]/ul/li[" + (aux) + "]//div [@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']/div/div/parent::div/span[1]/span[@aria-hidden='true']")).getText();
                        if (empresa.indexOf("·") != -1)
                            empresa = empresa.substring(0, empresa.indexOf("·"));
                    }catch (Exception e1){
                        empresa="";
                    }
                    String fecha, fecha1, fecha2;
                    try {
                        fecha = driver.findElement(By.xpath("//div[@id='experience']//following-sibling::div[2]/ul/li[" + (aux) + "]//div [@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']/div/div/parent::div/span[2]/span[@aria-hidden='true']")).getText();
                        if (fecha.contains("-")) {
                            String fechaC[] = fecha.split("-");
                            fecha1 = fechaC[0].trim();
                            fecha2 = fechaC[1].trim();
                            if (fecha2.indexOf("·") != -1)
                                fecha2 = fecha2.substring(0, fecha2.indexOf("·"));
                        } else {
                            fecha1 = fecha;
                            fecha2 = "Actualidad";
                        }
                    } catch (Exception e1) {
                        fecha1 = "";
                        fecha2 = "";
                    }
                    experiencia = new Experiencia(empresa, cargo, fecha1, fecha2);
                    listExperiencia.add(experiencia);
                    i++;
                    aux++;
                } catch (Exception ex) {
                    try {
                        String experSize2 = String.valueOf(driver.findElements(By.xpath("//div[@id='experience']//parent::section/div/div//parent::div[@class='pvs-header__container']/following-sibling::div/ul/li/div/div/div[@class='pvs-list__outer-container']/ul/li/span/following-sibling::div//div[@class='display-flex flex-column full-width align-self-center']//a/parent::div/parent::div/parent::div/parent::li/parent::ul/parent::div/parent::div")).size());
                        for (int j = 0; j < Integer.parseInt(experSize2); j++) {
                            String tam = String.valueOf(driver.findElements(By.xpath("//div[@id='experience']//parent::section/div/div//parent::div[@class='pvs-header__container']/following-sibling::div/ul/li[" + aux + "]/div/div/div[@class='pvs-list__outer-container']/ul/li/span/following-sibling::div//div[@class='display-flex flex-column full-width align-self-center']//a/parent::div/parent::div/parent::div/parent::li/parent::ul/parent::div/parent::div//li")).size());
                            String empresa;
                            try{
                                empresa = driver.findElement(By.xpath("//div[@id='experience']//parent::section/div/div//parent::div[@class='pvs-header__container']/following-sibling::div/ul/li[" + aux + "]/div/div/div[@class='pvs-list__outer-container']/ul/li/span/following-sibling::div//div[@class='display-flex flex-column full-width align-self-center']//a/parent::div/parent::div/parent::div/parent::li/parent::ul/parent::div/parent::div/div[@class='display-flex flex-row justify-space-between']//span[@class='mr1 hoverable-link-text t-bold']/span[@aria-hidden='true']")).getText();
                                if (empresa.indexOf("·") != -1)
                                    empresa = empresa.substring(0, empresa.indexOf("·"));
                            }catch (Exception e1){
                                empresa="";
                            }
                            for (int m = 0; m < Integer.parseInt(tam); m++) {
                                String cargo;
                                try{
                                    cargo = driver.findElement(By.xpath("//div[@id='experience']//parent::section/div/div//parent::div[@class='pvs-header__container']/following-sibling::div/ul/li[" + aux + "]/div/div/div[@class='pvs-list__outer-container']/ul/li/span/following-sibling::div//div[@class='display-flex flex-column full-width align-self-center']//a/parent::div/parent::div/parent::div/parent::li/parent::ul/parent::div/parent::div//div[@class='pvs-list__outer-container']//li[" + (m + 1) + "]//span[@class='mr1 hoverable-link-text t-bold']/span[@aria-hidden='true']")).getText();
                                }catch (Exception e1){
                                    cargo="";
                                }
                                String fecha, fecha1, fecha2;
                                try {
                                    fecha = driver.findElement(By.xpath("//div[@id='experience']//parent::section/div/div//parent::div[@class='pvs-header__container']/following-sibling::div/ul/li[" + aux + "]/div/div/div[@class='pvs-list__outer-container']/ul/li/span/following-sibling::div//div[@class='display-flex flex-column full-width align-self-center']//a/parent::div/parent::div/parent::div/parent::li/parent::ul/parent::div/parent::div//div[@class='pvs-list__outer-container']//li[" + (m + 1) + "]//span[@class='t-14 t-normal t-black--light'][1]")).getText();
                                    if (fecha.contains("-")) {
                                        String fechaC[] = fecha.split("-");
                                        fecha1 = fechaC[0].trim();
                                        fecha2 = fechaC[1].trim();
                                        if (fecha2.indexOf("·") != -1)
                                            fecha2 = fecha2.substring(0, fecha2.indexOf("·"));
                                    } else {
                                        fecha1 = fecha;
                                        fecha2 = "Actualidad";
                                    }
                                } catch (Exception e1) {
                                    fecha1 = "";
                                    fecha2 = "";
                                }
                                experiencia = new Experiencia(empresa, cargo, fecha1, fecha2);
                                listExperiencia.add(experiencia);
                            }
                        }
                    } catch (Exception v) {
                    }
                    aux++;
                }
            } while (i < Integer.parseInt(experSize));
            Thread.sleep(3000);
        }catch(Exception e3){
            }
    }
    }
}