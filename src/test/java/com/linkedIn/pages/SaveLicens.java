package com.linkedIn.pages;

import Objects.Certificaciones;
import Objects.Educacion;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.awt.*;
import java.util.ArrayList;

public class SaveLicens {
    WebDriver driver;

    public SaveLicens(WebDriver remoteDriver) {
        driver = remoteDriver;
        PageFactory.initElements(driver, this);
    }

    public void Save(ArrayList<Certificaciones> listLicens) throws InterruptedException, AWTException {
        Certificaciones certificacion;
        Thread.sleep(3000);
        try {
            WebElement mas = driver.findElement(By.xpath("//div[@id='licenses_and_certifications']//parent::section/div/div[@class='pvs-list__footer-wrapper']//a"));
            mas.click();
            Thread.sleep(3000);
            String Size = String.valueOf(driver.findElements(By.xpath("//ul/li//div[@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']/a")).size());
            int i = 0, aux = 1;
            do {
                try {
                    String nombre = driver.findElement(By.xpath("//ul/li[" + (aux) + "]//div[@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']//div[@class='display-flex ']//span[@aria-hidden='true']")).getText();
                    String instituto = driver.findElement(By.xpath("//ul/li[" + (aux) + "]//div[@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']//span[@class='t-14 t-normal']//span[@aria-hidden='true']")).getText();
                    String expedicion;
                    try {
                        expedicion = driver.findElement(By.xpath("//ul/li[\" + (aux) + \"]//div[@class='display-flex flex-column full-width align-self-center']/div[@class='display-flex flex-row justify-space-between']//span[@class='t-14 t-normal t-black--light'][1]//span[@aria-hidden='true']")).getText();
                        if (expedicion.indexOf(":") != -1) {
                            expedicion = expedicion.substring(expedicion.lastIndexOf(":"));
                            expedicion = expedicion.substring(2);
                        }
                    }catch (Exception p){
                        expedicion="";
                    }
                    certificacion = new Certificaciones(nombre, instituto, expedicion);
                    listLicens.add(certificacion);
                    i++;
                    aux++;
                } catch (Exception e) {
                    aux++;
                }
            } while (i < Integer.parseInt(Size));
            Thread.sleep(3000);
            driver.navigate().back();
        } catch (Exception e) {
            try {
                String Size = String.valueOf(driver.findElements(By.xpath("//div[@id='licenses_and_certifications']//following-sibling::div[2]/ul/li//div[@class='display-flex flex-column full-width align-self-center']")).size());
                int i = 0, aux = 1;
                do {
                    try {
                        String nombre = driver.findElement(By.xpath("//div[@id='licenses_and_certifications']//following-sibling::div[2]/ul/li[" + (aux) + "]//div[@class='display-flex flex-column full-width align-self-center']//div[@class='display-flex flex-wrap align-items-center full-height']")).getText();
                        String instituto = driver.findElement(By.xpath("//div[@id='licenses_and_certifications']//following-sibling::div[2]/ul/li[" + (aux) + "]//div[@class='display-flex flex-column full-width align-self-center']//span[@class='t-14 t-normal']")).getText();
                        String expedicion;
                        try {
                            expedicion = driver.findElement(By.xpath("//div[@id='licenses_and_certifications']//following-sibling::div[2]/ul/li[" + (aux) + "]//div[@class='display-flex flex-column full-width align-self-center']//span[@class='t-14 t-normal t-black--light'][1]")).getText();
                            if (expedicion.indexOf(":") != -1) {
                                expedicion = expedicion.substring(expedicion.lastIndexOf(":"));
                                expedicion = expedicion.substring(2);
                            }
                        }catch (Exception p){
                            expedicion="";
                        }
                        certificacion = new Certificaciones(nombre, instituto, expedicion);
                        listLicens.add(certificacion);
                        i++;
                        aux++;
                    } catch (Exception ex) {
                        aux++;
                    }
                } while (i < Integer.parseInt(Size));
            } catch (Exception e2) {
            }
        }
    }
}