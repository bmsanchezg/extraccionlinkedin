package com.linkedIn.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.awt.*;
import java.util.ArrayList;

public class SaveKnow {
    WebDriver driver;

    public SaveKnow(WebDriver remoteDriver) {
        driver = remoteDriver;
        PageFactory.initElements(driver, this);
    }

    public void Save(ArrayList<String> listConocimiento) throws InterruptedException, AWTException {
        String conocimiento;
        Thread.sleep(3000);
        try {
            WebElement mas = driver.findElement(By.xpath("//div[@id='skills']//parent::section/div/div[@class='pvs-list__footer-wrapper']//a"));
            mas.click();
            Thread.sleep(3000);
            String experSize = String.valueOf(driver.findElements(By.xpath("//section/div[@id]/div[@class='artdeco-tabpanel active ember-view']//ul[@class='pvs-list ']/li//a[@data-field]//span[@aria-hidden='true']")).size());
            int i = 0, aux = 1;
            do {
                try {
                    conocimiento = driver.findElement(By.xpath("//div[@class='artdeco-tabpanel active ember-view']//div[@class='scaffold-finite-scroll__content']/ul/li[" + aux + "]/div/div/div[@class]/div/a//span[@aria-hidden='true']")).getText();
                    listConocimiento.add(conocimiento);
                    i++;
                    aux++;
                } catch (Exception e) {
                    aux++;
                }
            } while (i < Integer.parseInt(experSize));
            Thread.sleep(3000);
            driver.navigate().back();
        } catch (Exception e) {
            try {
                String experSize = String.valueOf(driver.findElements(By.xpath("//div[@id='skills']//following-sibling::div[2]/ul/li//div//div[@class='display-flex flex-row justify-space-between']/a")).size());
                int i = 0, aux = 1;
                do {
                    try {
                        conocimiento = driver.findElement(By.xpath("//div[@id='skills']//following-sibling::div[2]/ul/li[" + (aux) + "]//div//div[@class='display-flex flex-row justify-space-between']/a//span[@aria-hidden='true']")).getText();
                        listConocimiento.add(conocimiento);
                        i++;
                        aux++;
                    } catch (Exception ex) {
                        aux++;
                    }
                } while (i < Integer.parseInt(experSize));
            } catch (Exception e1) {
            }
        }
    }
}
