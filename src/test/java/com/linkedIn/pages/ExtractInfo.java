package com.linkedIn.pages;

import Objects.Certificaciones;
import Objects.Estudiante;
import Objects.Educacion;
import Objects.Experiencia;
import com.linkedIn.utils.WriteFile;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class ExtractInfo {
    WebDriver driver;
    SaveExperience saveExperience;
    SaveEducation saveEducation;
    SaveKnow saveKnow;
    SaveLicens saveLicens;


    public ExtractInfo(WebDriver remoteDriver) {
        driver = remoteDriver;
        PageFactory.initElements(driver, this);
    }

    public void List() throws InterruptedException, AWTException, IOException {
        WriteFile writeFile;
        ArrayList<Estudiante> listEstudiante = new ArrayList<>();
        Estudiante estudiante;

        ArrayList<Educacion> listEducacion;
        ArrayList<Experiencia> listExperiencia;
        ArrayList<Certificaciones> listCertificaciones;
        ArrayList<String> listConocimiento;

        String paginacion = "//button[@aria-label='Siguiente']";
        try {

            int aux=1;
            Actions actions=new Actions(driver);
            do {
                for (int i = 0; i < 10; i++) {
                    Thread.sleep(3000);
                    try {
                        WebElement person = driver.findElement(By.xpath("//li[" + (i + 1) + "]//a[@class='app-aware-link ']/span[@dir='ltr']"));
                        person.click();
                        Thread.sleep(3000);

                        String name = driver.findElement(By.xpath("//h1[@class='text-heading-xlarge inline t-24 v-align-middle break-words']")).getText();
                        String profesion = driver.findElement(By.xpath("//div[@class='mt2 relative']//div[@class='text-body-medium break-words']")).getText();

                        listExperiencia = new ArrayList<>();
                        listEducacion = new ArrayList<>();
                        listConocimiento = new ArrayList<>();
                        listCertificaciones = new ArrayList<>();

                        saveExperience = new SaveExperience(driver);
                        saveExperience.Save(listExperiencia);
                        saveEducation = new SaveEducation(driver);
                        saveEducation.Save(listEducacion);
                        saveLicens = new SaveLicens(driver);
                        saveLicens.Save(listCertificaciones);
                        saveKnow = new SaveKnow(driver);
                        saveKnow.Save(listConocimiento);

                        estudiante = new Estudiante(name, profesion, listExperiencia, listEducacion, listCertificaciones, listConocimiento);
                        listEstudiante.add(estudiante);
                        Thread.sleep(2000);
                        driver.navigate().back();
                    } catch (Exception e) {
                    }
                }
                writeFile = new WriteFile();
                writeFile.write(listEstudiante);
                try{
                    actions.scrollByAmount(0,2000).perform();
                    Thread.sleep(2000);
                    WebElement pagina = driver.findElement(By.xpath(paginacion));
                    pagina.click();
                }catch (Exception l){
                    aux=-2;
                }
            } while (aux>-1);
        }catch (Exception l){
        }
        //writeFile = new WriteFile();
        //writeFile.write(listEstudiante);
    }
}